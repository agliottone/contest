from django.contrib import admin
from django import forms

from . import models


class PrizeAdminForm(forms.ModelForm):

    class Meta:
        model = models.Prize
        fields = "__all__"


class PrizeAdmin(admin.ModelAdmin):
    form = PrizeAdminForm
    list_display = [
        "created",
        "name",
        "last_updated",
        "perday",
        "code",
    ]
    readonly_fields = [
        "created",
        "last_updated",
    ]


class ContestAdminForm(forms.ModelForm):

    class Meta:
        model = models.Contest
        fields = "__all__"


class ContestAdmin(admin.ModelAdmin):
    form = ContestAdminForm
    list_display = [
        "created",
        "start",
        "end",
        "active",
        "last_updated",
        "code",
        "name",
        "prize",
        "prizes_available"
    ]
    readonly_fields = [
        "created",
        "last_updated",

    ]


class WinAdminForm(forms.ModelForm):

    class Meta:
        model = models.Win
        fields = "__all__"


class WinAdmin(admin.ModelAdmin):
    form = WinAdminForm
    list_display = [
        "created",
        "last_updated",
        'date'
    ]
    readonly_fields = [
        "created",
        "last_updated",
    ]


class AttemptAdminForm(forms.ModelForm):

    class Meta:
        model = models.Attempt
        fields = "__all__"


class AttemptAdmin(admin.ModelAdmin):
    form = AttemptAdminForm
    list_display = [
        "date",
        "last_updated",
        "created",
    ]
    readonly_fields = [

        "last_updated",
        "created",
    ]


admin.site.register(models.Prize, PrizeAdmin)
admin.site.register(models.Contest, ContestAdmin)
admin.site.register(models.Win, WinAdmin)
admin.site.register(models.Attempt, AttemptAdmin)
