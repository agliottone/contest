from django.db import models
from django.urls import reverse
from PyProbs import Probability as pr
from datetime import date

class Prize(models.Model):

    # Fields
    created = models.DateTimeField(auto_now_add=True, editable=False)
    name = models.CharField(max_length=30)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    perday = models.IntegerField()
    code = models.CharField(max_length=30)

    class Meta:
        pass

    def __str__(self):
        return str(self.name)

    def get_absolute_url(self):
        return reverse("contest_Prize_detail", args=(self.pk,))

    def get_update_url(self):
        return reverse("contest_Prize_update", args=(self.pk,))


class Contest(models.Model):

    # Relationships
    users = models.ManyToManyField("auth.User",blank=True)
    prize = models.ForeignKey("contest.prize", on_delete=models.CASCADE)

    # Fields
    created = models.DateTimeField(auto_now_add=True, editable=False)
    end = models.DateField()
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    code = models.CharField(max_length=30,unique=True)
    start = models.DateField()
    name = models.CharField(max_length=30)

    @property
    def active(self):
      present = date.today()
      return (self.start <= present and self.end >= present)
      
    @property
    def prizes_available(self):
      present = date.today()
      perday = self.prize.perday
      winned = Win.objects.filter(contest=self,date=present).count()
      return perday - winned
      
    class Meta:
        pass

    def play(self):
        prob = 0.09
        present = date.today()
        p = pr()
        history = Attempt.objects.filter(contest=self,date=present).values_list('result', flat=True)
        p.history= {prob: list(history)}
        result = p.iProb(prob)
        if result:
          win = Win(contest=self,date=present)
          win.save()
          return win
        else:
          return False
      
    def __str__(self):
        return str(self.name)

    def get_absolute_url(self):
        return reverse("contest_Contest_detail", args=(self.pk,))

    def get_update_url(self):
        return reverse("contest_Contest_update", args=(self.pk,))


class Win(models.Model):

    # Relationships
    contest = models.ForeignKey("contest.contest", on_delete=models.CASCADE)

    # Fields
    date = models.DateField(blank=True,null=True) 
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        pass

    def __str__(self):
        return str(self.pk)

    def get_absolute_url(self):
        return reverse("contest_Win_detail", args=(self.pk,))

    def get_update_url(self):
        return reverse("contest_Win_update", args=(self.pk,))


class Attempt(models.Model):

    # Relationships
    win = models.ForeignKey("contest.win", on_delete=models.SET_NULL,null=True)
    user = models.ForeignKey("auth.User", on_delete=models.SET_NULL, null=True)
    contest = models.ForeignKey("contest.contest", on_delete=models.CASCADE)

    # Fields
    date = models.DateField()
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    result = models.BooleanField(default=False)
    
    class Meta:
        pass

    def __str__(self):
        return str(self.pk)

    def get_absolute_url(self):
        return reverse("contest_Attempt_detail", args=(self.pk,))

    def get_update_url(self):
        return reverse("contest_Attempt_update", args=(self.pk,))
