from django.views import generic
from django.urls import reverse_lazy
from django.http.response import JsonResponse
from datetime import date
from . import models
from . import forms

def play(request):
    code = request.GET.get("code",None)
    if not code:
      response = JsonResponse({'error':{'status':400,'title':"Contest code missing","detail": "Specify contest code parameter."}}, status=400)
      return response
    try:
      contest = models.Contest.objects.get(code=code)
    except:
      response = JsonResponse({'error':{'status':404,'title':"Contest not found","detail": "Contest code " + code + " not found."}}, status=404)
      return response
    
    
    if not contest.active:
      response = JsonResponse({'error':{'status':422,'title':"Contest in not active","detail": "The contest with code " + code + " is not active."}}, status=422)
      return response
    
    if not contest.prizes_available:
      response = JsonResponse({ "data": { "winner": False, "prize": None,"detail": "Premi finiti" } }, status=200)
      return response
    
    present = date.today()
    attempt = models.Attempt(contest=contest,date=present)
    win = contest.play()
    if win:
      attempt.win = win
      attempt.save()
      response = JsonResponse({ "data": { "winner": True, "prize": { "code": contest.prize.code, "name": contest.prize.name } } }, status=200)
      return response
    else:
      attempt.save()
      response = JsonResponse({ "data": { "winner": False, "prize": None } }, status=200)
      return response



class PrizeListView(generic.ListView):
    model = models.Prize
    form_class = forms.PrizeForm


class PrizeCreateView(generic.CreateView):
    model = models.Prize
    form_class = forms.PrizeForm


class PrizeDetailView(generic.DetailView):
    model = models.Prize
    form_class = forms.PrizeForm


class PrizeUpdateView(generic.UpdateView):
    model = models.Prize
    form_class = forms.PrizeForm
    pk_url_kwarg = "pk"


class PrizeDeleteView(generic.DeleteView):
    model = models.Prize
    success_url = reverse_lazy("contest_Prize_list")


class ContestListView(generic.ListView):
    model = models.Contest
    form_class = forms.ContestForm


class ContestCreateView(generic.CreateView):
    model = models.Contest
    form_class = forms.ContestForm


class ContestDetailView(generic.DetailView):
    model = models.Contest
    form_class = forms.ContestForm


class ContestUpdateView(generic.UpdateView):
    model = models.Contest
    form_class = forms.ContestForm
    pk_url_kwarg = "pk"


class ContestDeleteView(generic.DeleteView):
    model = models.Contest
    success_url = reverse_lazy("contest_Contest_list")


class WinListView(generic.ListView):
    model = models.Win
    form_class = forms.WinForm


class WinCreateView(generic.CreateView):
    model = models.Win
    form_class = forms.WinForm


class WinDetailView(generic.DetailView):
    model = models.Win
    form_class = forms.WinForm


class WinUpdateView(generic.UpdateView):
    model = models.Win
    form_class = forms.WinForm
    pk_url_kwarg = "pk"


class WinDeleteView(generic.DeleteView):
    model = models.Win
    success_url = reverse_lazy("contest_Win_list")


class AttemptListView(generic.ListView):
    model = models.Attempt
    form_class = forms.AttemptForm


class AttemptCreateView(generic.CreateView):
    model = models.Attempt
    form_class = forms.AttemptForm


class AttemptDetailView(generic.DetailView):
    model = models.Attempt
    form_class = forms.AttemptForm


class AttemptUpdateView(generic.UpdateView):
    model = models.Attempt
    form_class = forms.AttemptForm
    pk_url_kwarg = "pk"


class AttemptDeleteView(generic.DeleteView):
    model = models.Attempt
    success_url = reverse_lazy("contest_Attempt_list")
