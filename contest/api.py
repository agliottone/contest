from rest_framework import viewsets, permissions
from rest_framework import status    
from rest_framework.response import Response

from . import serializers
from . import models

class PrizeViewSet(viewsets.ModelViewSet):
    queryset = models.Prize.objects.all()
    serializer_class = serializers.PrizeSerializer
    permission_classes = [permissions.IsAuthenticated]


class ContestViewSet(viewsets.ModelViewSet):
    queryset = models.Contest.objects.all()
    serializer_class = serializers.ContestSerializer
    permission_classes = [permissions.IsAuthenticated]


class WinViewSet(viewsets.ModelViewSet):
    queryset = models.Win.objects.all()
    serializer_class = serializers.WinSerializer
    permission_classes = [permissions.IsAuthenticated]


class AttemptViewSet(viewsets.ModelViewSet):
    queryset = models.Attempt.objects.all()
    serializer_class = serializers.AttemptSerializer
    permission_classes = [permissions.IsAuthenticated]
