from django.urls import path, include
from rest_framework import routers

from . import api
from . import views


router = routers.DefaultRouter()
router.register("Prize", api.PrizeViewSet)
router.register("Contest", api.ContestViewSet)
router.register("Win", api.WinViewSet)
router.register("Attempt", api.AttemptViewSet)

urlpatterns = (
    path("api/v1/", include(router.urls)),
    path("play/", views.play, name="play"),
    path("contest/Prize/", views.PrizeListView.as_view(), name="contest_Prize_list"),
    path("contest/Prize/create/", views.PrizeCreateView.as_view(), name="contest_Prize_create"),
    path("contest/Prize/detail/<int:pk>/", views.PrizeDetailView.as_view(), name="contest_Prize_detail"),
    path("contest/Prize/update/<int:pk>/", views.PrizeUpdateView.as_view(), name="contest_Prize_update"),
    path("contest/Prize/delete/<int:pk>/", views.PrizeDeleteView.as_view(), name="contest_Prize_delete"),
    path("contest/Contest/", views.ContestListView.as_view(), name="contest_Contest_list"),
    path("contest/Contest/create/", views.ContestCreateView.as_view(), name="contest_Contest_create"),
    path("contest/Contest/detail/<int:pk>/", views.ContestDetailView.as_view(), name="contest_Contest_detail"),
    path("contest/Contest/update/<int:pk>/", views.ContestUpdateView.as_view(), name="contest_Contest_update"),
    path("contest/Contest/delete/<int:pk>/", views.ContestDeleteView.as_view(), name="contest_Contest_delete"),
    path("contest/Win/", views.WinListView.as_view(), name="contest_Win_list"),
    path("contest/Win/create/", views.WinCreateView.as_view(), name="contest_Win_create"),
    path("contest/Win/detail/<int:pk>/", views.WinDetailView.as_view(), name="contest_Win_detail"),
    path("contest/Win/update/<int:pk>/", views.WinUpdateView.as_view(), name="contest_Win_update"),
    path("contest/Win/delete/<int:pk>/", views.WinDeleteView.as_view(), name="contest_Win_delete"),
    path("contest/Attempt/", views.AttemptListView.as_view(), name="contest_Attempt_list"),
    path("contest/Attempt/create/", views.AttemptCreateView.as_view(), name="contest_Attempt_create"),
    path("contest/Attempt/detail/<int:pk>/", views.AttemptDetailView.as_view(), name="contest_Attempt_detail"),
    path("contest/Attempt/update/<int:pk>/", views.AttemptUpdateView.as_view(), name="contest_Attempt_update"),
    path("contest/Attempt/delete/<int:pk>/", views.AttemptDeleteView.as_view(), name="contest_Attempt_delete"),
)
