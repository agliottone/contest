from rest_framework import serializers

from . import models


class PrizeSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Prize
        fields = [
            "created",
            "name",
            "last_updated",
            "perday",
            "code",
        ]

class ContestSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Contest
        fields = [
            "created",
            "end",
            "last_updated",
            "code",
            "start",
            "name",
        ]

class WinSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Win
        fields = [
            "created",
            "last_updated",
        ]

class AttemptSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Attempt
        fields = [
            "date",
            "last_updated",
            "created",
        ]
