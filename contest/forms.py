from django import forms
from contest import models


class PrizeForm(forms.ModelForm):
    class Meta:
        model = models.Prize
        fields = [
            "name",
            "perday",
            "code",
        ]


class ContestForm(forms.ModelForm):
    class Meta:
        model = models.Contest
        fields = [
            "end",
            "code",
            "start",
            "name",
            "users",
            "prize",
        ]


class WinForm(forms.ModelForm):
    class Meta:
        model = models.Win
        fields = [
            "contest",
        ]


class AttemptForm(forms.ModelForm):
    class Meta:
        model = models.Attempt
        fields = [
            "date",
            "win",
            "contest",
            "user",
        ]